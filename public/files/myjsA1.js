var xhr, extern=1, dado, count=0, ID, addr, localAddr='http://192.168.0.222/', externAddr='http://sartinrouter.ddns.net:8899/';

window.onload = function(){
	setInterval(function(){contador();}, 1000);
	if( $('#arduino').length == 1 )
		window.location.replace('http://alexsartin.gitlab.io/sartin/');
	else{
		adicionaInterface();
		$('#endereco').slideDown(1000);
		
		setupAjax(2000);
		xhr = $.get(localAddr+'stat', function(data){
			if(data == 'Working'){
				extern = 0;
				addr = localAddr;
				setTimeout(function(){checaSenha(1);}, 200);
			}
		}).fail(function(){
			setupAjax(120000);
			addr = externAddr;
			checaSenha(1);
		}).always(function(){
			updateInterface();
		});
	}	
};

function updateInterface(){
	if(extern){
		//Botao: Troca de endereço:
		$('<button id="btr" class="button btn btn-default">Atualizar dados</button>').appendTo('#alerta');
		$('#btr').click( function() {
			clearTimeout(timeOutError);
			refresh = false;
			$('#btc').html('Conectar');
			$('#endereco').slideDown(1000);
			$('#alerta').slideUp(1000);
		});
	}
	else
		$('#txt1').hide();
	
	$('#endereco h4:first').css("color", (extern ? "MidnightBlue" : "blue"))
		.text((extern ? 'Insira o endereço do Arduino:' : 'Rede interna detectada'));
		
	$('<h3> Sem Conexao com o Arduino '+ (extern ? 'no endereço:' : '') +
	'</h3><div id="end1"></div><div id="count"><h5></h5></div>').prependTo('#alerta');
}

function setupAjax(time1){
	$.ajaxSetup({ 
		timeout: time1,
		xhrFields: { withCredentials: true },
	});
}

// CONTADOR
function contador(value) {
	if(value){
		if(count<=0)
			count=value;	
	}
	else{
		if(count>0)
			$('#count h5').html("Atualizando em "+ count-- +"s");
		else 
			$('#count h5').html('<i class="fa fa-refresh fa-spin fa-2x"></i>');
	}
}

// INTERFACE
function adicionaInterface() {
	//Alerta: Sem conexão
	$('<div>', {
		id: 'alerta',
		class: 'caixa'
	}).insertAfter('a');
		
	//Input: Endereço
	$('<div>', {
		id: 'endereco',
		class: 'caixa',
		html: '<h4>Digite PIN:</h4><input type="password" id="txt2">'+
					'<button id="btc" class="button btn btn-default">Conectar</button>'
	}).insertAfter('#alerta');
	checaSenha();
	
	$('#endereco').prepend('<h4><i class="fa fa-spinner fa-spin"></i>  Procurando o Arduino...</h4><input type="text" value="'+externAddr+'" id="txt1">');

	//Online web-client status
	$('<h6></h6>').insertAfter('#endereco');

	//Versao:
	$('<h5>Sartin Automation v4.3</h5>').insertAfter('#endereco');
		
	//Enter no input
	$('#txt1, #txt2').keyup(function(event){
		if(event.keyCode == 13)
			$('#btc').focus().click();
	});
		
	//Aciona botão conectar
	$('#btc').click( function() {
		xhr.abort();
		if(extern){
			addr = $('#txt1').val();
			$('#end1').text(addr);
		}
		dado = $('#txt2').val();
		postSenha();
	});		
}

function checaSenha(_post){
	dado = getCookie('senha');
	if (dado !== ""){
		$('#txt2').val(dado);
		if(_post)	postSenha();
	}
}

// POST
function postSenha(){
	$('#btc').html('<i class="bt2 fa fa-refresh fa-spin"></i>');
	$.post(addr, { pass: dado }, function(response){
		if(response === ''){
			$('#btc').html('Conectar');
			$('#alerta').slideUp(700);
			$('#endereco').slideDown(900);
			$('#endereco h4:last').text('PIN incorreto!').css("color", "red");
			setTimeout(function(){ 
				$("#endereco h4:last").animate({ color: "black" }, 2500, function(){
					$(this).text('Digite o PIN:'); 
				});
			}, 2000);
		}
		else{		
			setCookie('senha', dado);
			ID = response;
			startRequest();
		}
	}).fail( function() {
		$('#btc').html('Conectar');
		$('#endereco h4:last').text('Falha na Conexão!').css("color", "purple");
		setTimeout(function(){ 
			$("#endereco h4:last").animate({ color: "black" }, 2500, function () {
				$(this).text('Digite o PIN:'); 
			});
		}, 2500);
	});
}

// COOKIES
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)===' ')
			c = c.substring(1);
		if (c.indexOf(name) === 0) 
			return c.substring(name.length, c.length);
	}
	return "";
}
function setCookie(name, value) {
	var d = new Date();
	d.setTime(d.getTime() + (3*12*30*24*60*60*1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = name + "=" + value + "; " + expires;
}