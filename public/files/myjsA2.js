var start=false, timeOutError, reles, code, hideReles=1, refresh=true, wait2refresh=false, medindo=false;
var devices=[0, 4, 3, 5, 6, 1, 10, 11], states=[0, 0, 0, 0, 0, 0, 0], tank_error=false, tanque, nivel=-1, timeOut=2000;

/////////////////////////// AQUISIÇAO DO ARDUINO /////////////////////////
function startRequest() {
	if(!start){
		//primeira aquisiçao p/ deixar o carregamento incial mais rapido
		setupAjax(15000);
		t1 = (new Date()).getTime();
		xhr = $.getJSON(addr+ID, function(data){
			td = (new Date()).getTime()-t1;
			if(td >= timeOut/2)
				timeOut = td+1500;
			atualizaDados(data);
		})
		.always(function(){
			setupAjax(timeOut); //Menor que o período de getRequest
			setInterval(function(){ getRequest(); }, timeOut+500);
		});
	}
	else{
		setupAjax(timeOut);
		refresh = true;
	}
}

function getRequest(){
	if(refresh){
		xhr = $.getJSON(addr+ID)
		.done(function(data){
			if(refresh) atualizaDados(data);
		})
		.fail(function(data){
			// Se nao foi abortado manualmente:
			if(data.statusText !== 'abort'){ 
			 	refresh = false;
			 	console.log("Falha na conexão"); 		
				$.get(addr, function(){
					setupAjax(120000);
					$.getJSON(addr+ID).done(function(data){
						atualizaDados(data);
						startRequest();
					}).fail(function(){
						$('#alerta h3').text('ID ou senha incorreta!');
						$('#end1').text('Atualize sua página');
						reiniciaContador();
						setTimeout(function(){ location.reload(); }, 5000);
					});
				}).fail(function(){
					$('#alerta h3').text('Sem conexao com o Arduino no endereço:');
					$('#end1').text(addr);
					reiniciaContador();
					if(addr == localAddr){
						$.getJSON(externAddr+ID).done(function(data){
							$('#alerta h3').text('Rede externa detectada!');
							$('#end1').text('Mudando para novo endereço...');
							addr = externAddr;
						});
					}
			  });	
			}
		});
	}
}

/////////////////////// ATUALIZACAO DOS ELEMENTOS DA PAGINA //////////////////////
function atualizaDados(data) {
	if(data){
		if(!start){
			//primeira leitura do Arduino:
			reles = data.b.length-2*hideReles;
			adicionaElementos();
			start = 1;
		}
		if(reles != data.b.length-2*hideReles)	//se o num mudou ou erro de leitura
			location.reload();

		// Checa se os valores dos alarmes alteraram (alarm residencial e cerca elétrica)
		if(data.b[4] != states[1] || data.b[3] != states[2])
			wait2refresh = false;

		// Ordem dos dados: Camera, Rele, LCD, Alarme, Cerca, Irrigação horta, Irrigação jardim; cd:error-code
		states = [data.b[0], data.b[4], data.b[3], data.b[5], data.b[6], data.b[1], data.b[2]];
		code = data.cd;

		// Erro no valor do tanque
		if(code == 504){
			if(!tank_error){
				tank_error = true;
				nivel = 0;
				tanque.refresh(0);
				setTimeout(function(){ 
					tanque.txtValue.attr({"text": "Erro"}); 
	 				tanque.txtLabel.attr({"text": ""});
				}, 1600);
			}
		}
		else if(nivel != data.cx) { //Alteracao no valor do tanque
			nivel = data.cx;
			tank_error = false;
			wait2refresh = false;
			tanque.refresh(nivel);
			setTimeout(function(){ tanque.txtLabel.attr({"text": (35*nivel).toString()+"L"}); }, 700);
		}
	}

	//Atualiza os elementos
	// wait2refresh: Espera atualizacao de estado da cerca/alarme
	if(!wait2refresh || i == 0){
		for(var i = 0; i <= reles; i++){
			if(states[i] == 0){
				$('[id=legenda] p').eq(i).text('(Desligad'+tEND[i]+')');
				$('[id=onOff]').eq(i).switchClass('fa-toggle-on', 'fa-toggle-off');
				$('[id=LED]').eq(i).removeClass().addClass('led-yellow');
			}
			else if(states[i] == 1){
				$('[id=legenda] p').eq(i).text('(Ligad'+tEND[i]+')');
				$('[id=onOff]').eq(i).switchClass('fa-toggle-off', 'fa-toggle-on');
				$('[id=LED]').eq(i).removeClass().addClass('led-green');
			}
			else{
				$('[id=legenda] p').eq(i).text('(DISPARAD'+tEND[i].toUpperCase()+')');
				$('[id=LED]').eq(i).removeClass().addClass('led-red');
			}
		}

	 	// Botão cx. dagua
	 	if(states[reles] == 1){
			$('[id=onOff]').eq(reles).switchClass('fa-toggle-off', 'fa-toggle-on');
			medindo_tank();
	 	}
		else{
			$('[id=onOff]').eq(reles).switchClass('fa-toggle-on', 'fa-toggle-off');
			$('[id=header] p').eq(reles).slideUp(1100);
			medindo = 0;
		}
	}

 	var erro = '';
 	if(code == 501)			erro = '(Relógio)';
 	else if(code == 502)	erro = '(Notificação)';
 	else if(code == 503 || code == 504)	erro = '(Cx. Dagua)';
 	else if(code !== 1 && code !== 200)	erro = code.toString();	
 	
 	$('h6').text('Status do Arduino: '+ ((erro=='')? 'OK' : ('Erro '+erro))).css('color', ( erro=='' ? 'black' : 'red'));
	//Mostra elementos:
	$('#alerta').slideUp(700);
	$('#endereco').slideUp(900);
	$('[id=lampada]').slideDown(1000);
}

////////////////////////////////// ADICIONA RELES //////////////////////////
var nomes = ["Câmeras", "Cerca Elétrica", "Alarme Residencial", "Irrigação Horta", "Irrigação Jardim", "Display LCD", "Relé 3"];
var icons = ["camera", "bolt", "child", "leaf", "leaf", "desktop", "lightbulb-o"];
var tEND  = ["as", "a", "o", "a", "a", "o", "o"];

function adicionaElementos() {	
	// Tanque:
	$('<div>', {
			id: 'lampada',
			class: 'caixa',
			html: '<div id="tanque"></div><div id="header"><p></p>'+
			'<button id="botao" class="btn btn-default btn-lg">'+
			'<i class="di fa fa-tint"></i> <i id="onOff" class="fa fa-toggle-on"></i></button></div>'
		}).insertAfter('a:first');

	tanque = new JustGage({
    id: "tanque",
    title: "Caixa D'água",
    decimals: 1,
    titleFontColor: "#337ab7",
    levelColors: ["#FF0000", "#ECFF00", "#00A1FF"]
  });

	//Reles:
	var index = nomes.length - 2*hideReles;
	for(var i = 1; i <= reles; i++){
		$('<div>', {
			id: 'lampada',
			class: 'caixa',
			html: '<div id="legenda"><div id="header"><a>'+nomes[index-i]+'</a><div id="LED"></div><p></p></div></div>'+
			'<button id="botao" class="btn btn-default btn-lg">'+
			'<i class="di fa fa-'+icons[index-i]+'"></i> <i id="onOff" class="fa fa-toggle-on"></i></button>'
		}).insertAfter('a:first');
	}

	//Ação dos botoes:
	$('[id=botao]').each(function(index, ui){
		$(ui).click(function(){
			// wait2refresh: Impede acionamento da cerca/alarme/tanque em possível transição
			if(!wait2refresh || index == 0){
				postBotao(index);
				if($('[id=onOff]').eq(index).hasClass('fa-toggle-on')){
					$('[id=onOff]').eq(index).switchClass('fa-toggle-on', 'fa-toggle-off');
				}
				else{
					$('[id=onOff]').eq(index).switchClass('fa-toggle-off', 'fa-toggle-on');
				}
				if(index == 1 || index == 2 || index == reles){ // Cerca, Alarme e Cx.
					wait2refresh = true;
					if(index == reles)
						medindo_tank();
					else{
						$('[id=header] p').eq(index).empty().append('<i class="fa fa-cog fa-spin"></i>');
						setTimeout(function(){ 
							wait2refresh=false;
							atualizaDados(); 
						}, 6000);
					}
				}
			}
		});	
	});

	// Acao dos títulos (headers)
	$("[id=header] a:contains('Irrigação')").each( function (index,ui) {
		$(ui).click( function () {
			var acao = states[index+3] < 2 ? "LIGAR" : "DESLIGAR";
			var artigo = index == 0 ? " da " : " do ";
			var result = confirm("Deseja " + acao + " a " + nomes[index+3].replace(" ", artigo) + " agora ?");
			if (result == true) {
				postBotao(index + reles + 1);
			} 
		});
	});
}

function medindo_tank() {
	if(!medindo){
		medindo = 1;
		$('[id=header] p').eq(reles).empty().append('<i class="fa fa-cog fa-spin"></i>');
		$('[id=header] p').eq(reles).slideDown(500);
		tanque.txtLabel.attr({"text": "MEDINDO"});
		tanque.refresh(nivel);
		setTimeout(function(){ 
			wait2refresh = false;
			tank_error = 0;
			nivel = -1;
		}, 3000);
	}
}

function postBotao(num){
	xhr.abort();
	refresh = false;
	$.post(addr+ID, { n: devices[num] }, function(data){ 
		atualizaDados(data);
	}, 'json')
	.always(function(){
		refresh = true;
	});
}

// REINICIA CONTADOR NA FALHA
function reiniciaContador(){
	$('[id=lampada]').slideUp(900);
	$('#endereco').slideUp(700);
	$('#alerta').delay(900).slideDown(700);
	contador(5);
	refresh = false;
	clearTimeout(timeOutError);
	timeOutError = setTimeout(function(){ refresh = true; }, 5000);
}